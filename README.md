Role Name
=========

This role will install Ansible Tower on a RHEL8 system.

Requirements
------------

System needs to be on RHEL >=8  with minimum 8GB of RAM. System should be pre-patched.

Role Variables
--------------

Default/main.yml needs to have the "my_tower_version" set to the correct version. This role will download the latest version, but when it cd's into the directory after untaring, it needs to know the correct version.
Change the other default values as needed/desired.

Dependencies
------------

This role requires a tower-backup-latest.tar.gz file in the files directory, even if you don't want to restore the from a backup. 
If you want a fresh install of Tower, put "restore" in the skip tags of your template.
The vars/tower_creds.yml files is ansible vault encrypted. Replace it with your own with an "admin_password" variable defined. This is the password that will be used in the inventory file for the setup.sh. If you want different passwords for Tower and PostgreSQL you will neded to create a new variable for PostgreSQL in the inventory install file.

Example Playbook
----------------

Below is an example playbook. 
ansible_host and ansible_host_id variables need to be provided somewhere, ie playbook, vars/main.yml or extra variables. This would be a user on the RHEL instance with sudoer rights.

---
- hosts: all
  gather_facts: false
  vars:
    ansible_host:
    ansible_host_id
  roles:
    - {role: tower-standalone}

License
-------

BSD

Author Information
------------------

This role is an update of work on Chris Tjon's repository found at:
https://github.com/ctjon/lab-playbooks/tree/master/roles/tower-standalone